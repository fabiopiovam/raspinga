// Monitor com 3 sensores de nível e 3 leds indicadores de status
// 



#define sensorvermelho 8
#define sensoramarelo 9
#define sensorverde 10

int valor_s1 = 1, valor_s2 = 1, valor_s3 = 1;



void setup()
{
  Serial.begin(9600);

//  Baud deriva do sobrenome de Émile Baudot, 
//  francês inventor do código telegráfico Baudot. 
//  Um baud é uma medida de velocidade de sinalização 
//  e representa o número de mudanças na linha de transmissão 
//  (seja em frequência, amplitude, fase etc...) ou eventos por segundo.
//Para se determinar a taxa de transmissão de um canal em bits por segundo - bps, 
//deve ser levado em consideração o tipo de codificação utilizada, 
//além da velocidade de sinalização (medida em Bauds) do canal de comunicação.https://pt.wikipedia.org/wiki/Baud

// 1 baud = 1 pulso por segundo One baud was equal to one pulse per second, a more robust measure as word length can vary.

//Example: Communication at the baud rate 1000 Bd means communication by means of sending 1000 symbols per second. 
//In the case of a modem, this corresponds to 1000 tones per second; similarly, in the case of a line code, 
//this corresponds to 1000 pulses per second. The symbol duration time is 1/1000 second (that is, 1 millisecond).https://en.wikipedia.org/wiki/Baud





  //Define os pinos dos sensores
  pinMode(sensorvermelho, INPUT);
  pinMode(sensoramarelo, INPUT);
  pinMode(sensorverde, INPUT);

  // define os pinos dos les indicadores

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(13, OUTPUT);


}

void loop()
{
  //Leitura dos sensores
  int valor_s1 = digitalRead(sensorvermelho);
  int valor_s2 = digitalRead(sensoramarelo);
  int valor_s3 = digitalRead(sensorverde);

  //Mostra valor dos sensores no serial monitor
  Serial.print("S1: ");
  Serial.print(valor_s1);
  Serial.print(" S2: ");
  Serial.print(valor_s2);
  Serial.print(" S3: ");
  Serial.println(valor_s3);

  //Checa o nivel
  if ((valor_s1 == 1) && valor_s2 == 1 && valor_s3 == 1)
  {



    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(13, LOW);


  }

  if ((valor_s1 == 0) && valor_s2 == 1 && valor_s3 == 1)
  {


    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(13, LOW);



  }

  if ((valor_s1 == 0) && valor_s2 == 0 && valor_s3 == 1)
  {


    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(13, LOW);



  }

  if ((valor_s1 == 0) && valor_s2 == 0 && valor_s3 == 0)
  {

    digitalWrite(13, HIGH);
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);

  }
  delay(100);
}
