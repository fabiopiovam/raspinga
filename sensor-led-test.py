from gpiozero import LED, Button
from time import sleep

led1 = LED(17)
led2 = LED(27)
led3 = LED(22)

btn1 = Button(26)
btn2 = Button(19)

while True:
    if btn1.is_pressed:
        led1.on()
    else:
        led1.off()

    if btn2.is_pressed:
        led2.on()
    else:
        led2.off()

